#include "graph.h"
#include "queue.h"
#include "list.h"
Graph *allocGraph()
{
    Graph *self = (Graph *)(malloc(sizeof(Graph)));
    self->list = List_alloc();
    return self;
}

void freeGraph(Graph *self)
{
    Node *nodeList;
    int index = 0;
    for (int i = 0; i < self->list->size; i++)
    {
        nodeList = (Node *)self->list->items[i];
        index = i;
        Node *nodeForDelete;
        while (nodeList != NULL)
        {
            nodeForDelete = nodeList;
            nodeList = nodeList->next;
            free(nodeForDelete);
        }
    }
    List_free(self->list);
    free(self);
}

bool existEdge(Graph *self, int keyValueOut, int keyValueIn)
{
    if (existVertex(self, keyValueOut) == false)
        return false;
    if (existVertex(self, keyValueIn) == false)
        return false;
    Node *outNode = getVertex(self, keyValueOut);
    while (outNode->next != NULL)
    {
        outNode = outNode->next;
        if (outNode->keyValue == keyValueIn)
        {
            return true;
        }
    }
    return false;
}
bool existVertex(Graph *self, int keyValue)
{
    for (int i = 0; i < self->list->size; i++)
    {
        if (((Node *)self->list->items[i])->keyValue == keyValue)
        {
            return true;
        }
    }
    return false;
}
int addVertex(Graph *self, int keyValue)
{
    if (existVertex(self, keyValue) == true)
        return 1;

    Node *node = createNode(keyValue);
    List_add(self->list, node);
    return 0;
}
int addEdge(Graph *self, int keyValueOut, int keyValueIn)
{
    if (existEdge(self, keyValueOut, keyValueIn) == true)
        return 1;
    if (existVertex(self, keyValueOut) == false)
    {
        addVertex(self, keyValueOut);
    }
    if (existVertex(self, keyValueIn) == false)
    {
        addVertex(self, keyValueIn);
    }
    Node *outNode = getVertex(self, keyValueOut);
    addTolist(outNode, keyValueIn);
    Node *inNode = getVertex(self, keyValueIn);
    addTolist(inNode, keyValueOut);
    return 0;
}

bool removeEdge(Graph *self, int keyValueOut, int keyValueIn)
{
    if (existEdge(self, keyValueOut, keyValueIn) == false)
        return false;
    Node *outNode = getVertex(self, keyValueOut);
    while (outNode->next->keyValue != keyValueOut)
    {
        outNode = outNode->next;
    }
    Node *NodeforDelete = outNode->next;
    outNode->next = outNode->next->next;
    free(NodeforDelete);
    return true;
}
bool removeVertex(Graph *self, int keyValue)
{
    if (existVertex(self, keyValue) == false)
        return false;

    Node *nodeList;
    int index = 0;
    for (int i = 0; i < self->list->size; i++)
    {
        if (((Node *)self->list->items[i])->keyValue == keyValue)
        {
            nodeList = (Node *)self->list->items[i];
            index = i;
        }
    }
    Node *nodeForDelete;
    while (nodeList != NULL)
    {
        nodeForDelete = nodeList;
        nodeList = nodeList->next;
        free(nodeForDelete);
    }
    for (int i = index; i < self->list->size - 1; i++)
    {
        self->list->items[i] = self->list->items[i + 1];
        self->list->size = self->list->size - 1;
    }
}
// add adjacent vertex to list of vertexIn
void addTolist(Node *head, int keyValueIn)
{
    Node *bufNode = head;
    while (bufNode->next != NULL)
    {
        bufNode = bufNode->next;
    }
    bufNode->next = createNode(keyValueIn);
}

Node *createNode(int keyValue)
{
    Node *node = (Node *)(malloc(sizeof(Node)));
    node->keyValue = keyValue;
    node->next = NULL;
    node->visited = false;
    return node;
}
void makeVisited(Graph *self, int keyValue)
{
    if (existVertex(self, keyValue) == false)
        return;
    for (int i = 0; i < self->list->size; i++)
    {
        if (((Node *)self->list->items[i])->keyValue == keyValue)
        {
            ((Node *)self->list->items[i])->visited = true;
        }
    }
}


void BFS(Graph *self, int keyValue)
{
    if (existVertex(self, keyValue) == false)
        return;
      QueueCircular queue;
    QueueCircular_init(&queue);
   
    
    QueueCircular_insert(keyValue,&queue);
    while (!QueueCircular_isEmpty(&queue))
    {
        int value = QueueCircular_pop(&queue);
        makeVisited(self, value);
        Node *ourVertex = NULL;
        for (int i = 0; i < self->list->size; i++)
        {
            if (((Node *)self->list->items[i])->keyValue == value)
            {
                ourVertex = static_cast<Node *>(self->list->items[i]);
                ourVertex->parent=value;
            }
        }
        ourVertex = ourVertex->next;
        while(ourVertex!=NULL)
        {
            int checkVisit=getValue(ourVertex);
            if(isVisited(self,checkVisit)==false)
            {
                QueueCircular_insert(getValue(ourVertex), &queue);
            }
            ourVertex = ourVertex->next;
        }
    }
    QueueCircular_deinit(&queue);
}
Node *getVertex(Graph *self, int keyValue)
{
    Node *outNode = NULL;
    for (int i = 0; i < self->list->size; i++)
    {
        if (((Node *)self->list->items[i])->keyValue == keyValue)
        {
            outNode = (Node *)self->list->items[i];
        }
    }
    return outNode;
}
int getValue(Node *node)
{
    return node->keyValue;
}

bool isVisited(Graph* self,int keyValue)
{
    if(existVertex(self,keyValue)==false)
        return false;
    for(int i=0;i<self->list->size;i++)
    {
        if(static_cast<Node *>(self->list->items[i])->keyValue==keyValue)
        {
            if(((Node *)self->list->items[i])->visited==true)
            {
                return true;
            }
        }

    }
    return false;   
}

bool isCoherentGraph(Graph* self,int keyValue)
{
    BFS(self,keyValue);
    for(int i=0;i<self->list->size;i++)
    {
        if(((Node*)self->list->items[i])->visited==false)
        {
            return false;
        }
    }
    return true;
}
void cancelVisits(Graph * self)
{
    for(int i=0;i<self->list->size;i++)
    {
        if(((Node *)self->list->items[i])->visited==true)
        {
           ((Node *)self->list->items[i])->visited=false;
        }
    }
}
