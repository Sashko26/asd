#pragma once
#include <stdio.h>
#include "list.h"
#include "queue.h"


struct Node
{
    int keyValue;
    struct Node *next;
    bool visited;
    int parent;
};typedef struct Node Node;


struct Graph
{
    List *list;
};
typedef struct Graph Graph;

int addVertex(Graph *self, int keyValue);
int addEdge(Graph *self, int keyValueOut, int keyValueIn);
bool removeEdge(Graph *self, int keyValueOut, int keyValueIn);
bool removeVertex(Graph *self, int keyValue);
bool existEdge(Graph *self, int keyValueOut, int keyValueIn);
bool existVertex(Graph *self, int keyValue);
void freeGraph(Graph *self);
void makeVisited(Graph* self,int keyValue);
bool isVisited(Graph* self,int keyValue);
Node* getVertex(Graph *self,int keyValue);
void BFS(Graph *self, int keyValue);

Graph *allocGraph();
void addTolist(Node *head, int keyValueIn);
Node *createNode(int keyValue);
bool isCoherentGraph(Graph* self,int keyValue);
int getValue(Node *node);
void cancelVisits(Graph * self);
