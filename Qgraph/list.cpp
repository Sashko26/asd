#include "list.h"
void List_init(List *self)
{
    self->capacity = 4;
    self->size = 0;
    self->items = static_cast<void **>(malloc(sizeof(void *) * self->capacity));
    if (self->items == NULL)
    {
        fprintf(stderr, "пам'яті немає!\n");
        abort();
    }
}
void List_realloc(List *self)
{
    int newCapacity = (int)self->capacity * 2;
    self->items = static_cast<void **>(realloc(self->items, sizeof(void *) * newCapacity));
    self->capacity = newCapacity;
    if (self->items == NULL)
    {
        fprintf(stderr, "пам'яті немає!\n");
        abort();
    }
}
//звільнення динамічно виділеної пам'яті під items
void List_deinit(List *self)
{
    free(self->items);
}
// виділення динамічної пам'яті для  списку і його ініціалізація
List *List_alloc(void)
{
    List *self = static_cast<List *>(malloc(sizeof(List)));
    List_init(self);
    return self;
}
// звілнення динамічно виділеної пам'яті під items і під структуру
void List_free(List *self)
{
    List_deinit(self);
    free(self);
}
// отримати значення рядок зі списку
void *List_get(List *self, int index)
{
    return self->items[index];
}
// присвоїти нове значення для елменту списку(рядку) за індексом
void List_set(List *self, int index, void *value)
{
    void *bufValue = self->items[index];
    self->items[index] = value;
    free(bufValue);
}
// вставити рядок в список
void List_insert(List *self, int index, void *value)
{
    if (self->capacity > self->size)
    {
        int iterator = (int)(self->size) - 1;
        int i = iterator;
        for (; i >= index; i--)
        {
            if (i != index)
            {
                self->items[i + 1] = self->items[i];
            }
            else
            {
                self->items[i + 1] = self->items[i];
                self->items[i] = value;
                self->size = self->size + 1;
            }
        }
    }
    else if (self->capacity <= self->size)
    {
        List_realloc(self);

        for (int i = self->size - 1; i >= index; i--)
        {
            if (i != index)
            {
                self->items[i + 1] = self->items[i];
            }
            else
            {
                self->items[i + 1] = self->items[i];
                self->items[i] = value;
                self->size = self->size + 1;
            }
        }
    }
}
// видалити рядок в списку за індексом
void List_removeAt(List *self, int index)
{
    if (index >= self->size)
    {
        fprintf(stderr, "Element with this index don't exist!");
    }
    else
    {
        free(self->items[index]);
        int i = index;
        for (; i < (int)self->size - 1; i++)
        {
            self->items[i] = self->items[index + 1];
        }
        self->size = self->size - 1;
    }
}
// добавити рядок в список
void List_add(List *self, void *value)
{

    if (self->size >= self->capacity)
    {
        List_realloc(self);
        self->items[self->size] = value;
        self->size += 1;
    }
    else
    {
        self->items[self->size] = value;
        self->size += 1;
    }

    // @todo resize
}
// видалити зі списку рядок за значенням
void List_remove(List *self, void *value)
{
    int flag = self->size;
    for (int i = 0; i < self->size; i++)
    {
        if (self->items[i] == value)
        {
            free(self->items[i]);
            for (int j = i; j < self->size - 1; j++)
            {
                self->items[j] = self->items[j + 1];
            }
            self->size = self->size - 1;
        }
    }
    if (flag == self->size)
    {
        fprintf(stderr, "Цього значення не має в заданому списку!");
    }
}
// визначити індекс рядка за його значенням(вказівником по-суті) в списку

// визначити чи містить список такий вказівник
bool List_contains(List *self, void *value)
{
    for (int i = 0; i < self->size; i++)
    {
        if (self->items[i] == value)
        {
            return true;
        }
    }
    return false;
}
// визначити чи є список пустим
bool List_isEmpty(List *self)
{
    if (self->size == 0)
    {
        return true;
    }
    return false;
}
// почистити список від рядків
void List_clear(List *self)
{
    for (int i = 0; i < self->size; i++)
    {
        if(self->items[i]!=NULL)
        {
            free(self->items[i]);
        }
    }
    self->size = 0;
}
