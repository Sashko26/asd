
//реалізація неорієнтованого графа з пошуком у ширину.

#include <QCoreApplication>

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

#include "list.h"
#include "graph.h"
#include "queue.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    // Початок програми
     Graph * graph=allocGraph();
     addVertex(graph,1);
     addVertex(graph,2);
     addEdge(graph,1,2);
     BFS(graph,1);

      if(isCoherentGraph(graph,1)==true)
      {
          puts("Graph is coherent");
      }
       cancelVisits(graph);
      if(isCoherentGraph(graph,2)==true)
      {
          puts("Graph is coherent");
      }
        freeGraph(graph);
            return a.exec();
}
