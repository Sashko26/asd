// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include "queue.h"


void QueueCircular_init(QueueCircular *self)
{
    self->capacity = 4;
    self->array =static_cast<int *>(malloc(sizeof(int) * self->capacity));
    self->indexHead = 0;
    self->indexLast = 0;
    self->size = 0;
    self->parentIndex=0;
};
void QueueCircular_realloc(QueueCircular *self)
{
    self->capacity = self->capacity * 2;
    self->array =static_cast<int *>(realloc(self->array, sizeof(int) * self->capacity));
    if (self->indexHead != 0)
    {
        int bufFromHeaad[self->size];
        int bufFromLast[self->size];
        for (int i = self->indexHead, k = 0; i < self->size; i++, k++)
        {
            bufFromHeaad[k] = self->array[i];
        }
       

        for (int i = 0; i <= self->indexLast; i++)
        {
            bufFromLast[i] = self->array[i];
        }
        for (int i = 0; i < self->size-self->indexHead; i++)
        {
            self->array[i]=bufFromHeaad[i];
        }
        for (int i = self->size-self->indexHead,k=0;i < self->size; i++,k++)
        {
            self->array[i]=bufFromLast[k];

        }
    }
    self->indexHead=0;
    self->indexLast=self->size-1;
}
void QueueCircular_deinit(QueueCircular *self)
{
    free(self->array);
}
void QueueCircular_insert(int value, QueueCircular *self)
{
    if (self->capacity <= self->size)
        QueueCircular_realloc(self);

    if (self->size > 0)
    {
        self->indexLast = (self->indexLast + 1) % self->capacity;
    }
    self->size += 1;
    self->array[self->indexLast] = value;
}
int QueueCircular_pop(QueueCircular *self)
{
    if (self->size == 0)
        return -1;
        
        int value=self->array[self->indexHead];
    
    if (self->size == 1)
    {
        self->size=0;
        return value;
    }
    else
    {
        self->indexHead = (self->indexHead + 1) % self->capacity;
        self->size -= 1;

        return value;
    }
}
void QueueCircular_print(QueueCircular *self)
{
    if(self->size==0)
    return;

    int i = self->indexHead;
    while (i != self->indexLast)
    {
        printf("|%i| ", self->array[i]);
        i = (i + 1) % self->capacity;
    }    
     printf("|%i| ", self->array[self->indexLast]);
    puts("");
}

bool QueueCircular_isEmpty(QueueCircular *self)
{
    if(self->size==0)
    return true;
    return false;
}
