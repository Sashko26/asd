#pragma once
#include <stdlib.h>
#include <stdbool.h>
 struct QueueCircular
{   
    int *array;
    int capacity;
    int size;
    int indexHead;
    int indexLast;
    int parentIndex;
};typedef struct QueueCircular QueueCircular;

void QueueCircular_init(QueueCircular *self);
void QueueCircular_realloc(QueueCircular *self);
void QueueCircular_deinit(QueueCircular *self);
void QueueCircular_insert(int value, QueueCircular *self);
int QueueCircular_pop(QueueCircular *self);
void QueueCircular_print(QueueCircular *self);
bool QueueCircular_isEmpty(QueueCircular *self);
