#include <stdio.h>
#include <stdlib.h>
#include "binaryHeap.h"

int findMax(BinaryHeap *self)
{
    return self->arr[0];
}
void init(BinaryHeap *self)
{
    self->capacity = 8;
    self->arr = malloc(sizeof(int) * self->capacity);
    self->heapSize = 0;
}
void deinit(BinaryHeap *self)
{
    free(self->arr);
    self->heapSize = 0;
    self->capacity = 0;
}
void insert(BinaryHeap *self, int value)
{
    if (self->heapSize >= self->capacity)
    {
        heapRealloc(self);
    }
    self->arr[self->heapSize] = value;
    self->heapSize += 1;
    heapify(self);
}
int deleteRoot(BinaryHeap *self)
{   
    int buf=self->arr[0];
    self->arr[0]=self->arr[self->heapSize-1];
    self->heapSize=self->heapSize-1;
    heapify(self);
    return buf;
    
}
int extractMax(BinaryHeap *self)
{
    int buf = self->arr[0];
    self->arr[0] = self->arr[self->heapSize - 1];
    self->heapSize = self->heapSize - 1;
    return buf;
}
void heapify(BinaryHeap *self)
{
    if (self->heapSize % 2 == 0)
    {
        for (int i = 0; i < (self->heapSize - 2)/2; i++)
        {
            if (self->arr[i * 2 + 1] > self->arr[i] || self->arr[i * 2 + 2] > self->arr[i])
            {
                if (self->arr[i * 2 + 1] > self->arr[i] && self->arr[i * 2 + 2] <= self->arr[i])
                {
                    int buf = self->arr[i];
                    self->arr[i] = self->arr[i * 2 + 1];
                    self->arr[i * 2 + 1] = buf;
                }
                else if (self->arr[i * 2 + 2] > self->arr[i] && self->arr[i * 2 + 1] <= self->arr[i])
                {
                    int buf = self->arr[i];
                    self->arr[i] = self->arr[i * 2 + 2];
                    self->arr[i * 2 + 2] = buf;
                }
                else if (self->arr[i * 2 + 1] > self->arr[i] && self->arr[i * 2 + 2] > self->arr[i])
                {
                    if (self->arr[i * 2 + 1] >= self->arr[i * 2 + 2])
                    {
                        int buf = self->arr[i];
                        self->arr[i] = self->arr[i * 2 + 1];
                        self->arr[i * 2 + 1] = buf;
                    }
                    else
                    {
                        int buf = self->arr[i];
                        self->arr[i] = self->arr[i * 2 + 2];
                        self->arr[i * 2 + 2] = buf;
                    }
                }
            }
        }
        if(self->arr[(self->heapSize - 2)/2]<self->arr[self->heapSize-1])
        {
            int buf=self->arr[(self->heapSize - 2)/2];
            self->arr[(self->heapSize - 2)/2]=self->arr[self->heapSize-1];
            self->arr[self->heapSize-1]=buf;
        }
    }
    else
    {
         for (int i = 0; i <= (self->heapSize - 2)/2; i++)
        {
            if (self->arr[i * 2 + 1] > self->arr[i] || self->arr[i * 2 + 2] > self->arr[i])
            {
                if (self->arr[i * 2 + 1] > self->arr[i] && self->arr[i * 2 + 2] <= self->arr[i])
                {
                    int buf = self->arr[i];
                    self->arr[i] = self->arr[i * 2 + 1];
                    self->arr[i * 2 + 1] = buf;
                }
                else if (self->arr[i * 2 + 2] > self->arr[i] && self->arr[i * 2 + 1] <= self->arr[i])
                {
                    int buf = self->arr[i];
                    self->arr[i] = self->arr[i * 2 + 2];
                    self->arr[i * 2 + 2] = buf;
                }
                else if (self->arr[i * 2 + 1] > self->arr[i] && self->arr[i * 2 + 2] > self->arr[i])
                {
                    if (self->arr[i * 2 + 1] >= self->arr[i * 2 + 2])
                    {
                        int buf = self->arr[i];
                        self->arr[i] = self->arr[i * 2 + 1];
                        self->arr[i * 2 + 1] = buf;
                    }
                    else
                    {
                        int buf = self->arr[i];
                        self->arr[i] = self->arr[i * 2 + 2];
                        self->arr[i * 2 + 2] = buf;
                    }
                }
            }
        }

    }
    
}
void heapRealloc(BinaryHeap *self)
{
    self->capacity = self->capacity * 2;
    self->arr = realloc(self->arr, sizeof(int) * self->capacity);
}
void printHeap(BinaryHeap* self)
{
    for(int i=0;i<self->heapSize;i++)
    {
        printf("|%i| ",self->arr[i]);
    }
    puts("");

}
int* heapSort(int * arr, int size)
{
    //build bynary heap from all elements of array
    BinaryHeap tree;
    init(&tree);
     int buf[size];
    for(int i=0;i<size;i++)
    {
        insert(&tree,arr[i]);
    }
    for(int i=0;i<size;i++)
    {
       buf[i] = deleteRoot(&tree);
       arr[size-1-i]=buf[i];
    }
    deinit(&tree);
    return arr;
}
