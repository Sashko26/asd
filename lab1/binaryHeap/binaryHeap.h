#include<stdio.h>
#include<stdlib.h>
struct BinaryHeap
{
    int* arr;
    int capacity;
    int heapSize;
};
typedef struct BinaryHeap BinaryHeap;
int findMax();
void init(BinaryHeap* self);
void deinit(BinaryHeap* self); 
void insert(BinaryHeap* self,int value); 
int  extractMax(BinaryHeap* self);
void heapify(); 
void heapRealloc(BinaryHeap* self);
void heapRealloc(BinaryHeap* self);
void printHeap(BinaryHeap* self);
int deleteRoot(BinaryHeap *self);
int* heapSort(int * arr, int size);