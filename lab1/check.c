 // Компілювати за допомогою:
 // gcc main.c -lprogbase -lm
 
 #include <stdio.h>    // Для друку в термінал
 #include <math.h>     // Для математичних функцій
 #include <stdlib.h>   // Деякі додаткові функції
 #include <progbase.h> // Спрощений ввід і вивід даних у консоль
 
 int main() 
 {
     int array[5];
     for(int i=0;i<5;i++)
     {
         array[i]=rand()%10;
         printf("|%i |",array[i]);
     }
     puts("");
     int* arr=&array[2];
     printf("|%i|\n ",arr[1]);
     printf("|%i|\n ",array[3]);

     return 0;
 }