#include <iostream>
#include <string>

struct circularArray
{

    int *array;
    int capacity;
    int size;
    int indexHead;
    int indexLast;
};
void init(circularArray *self)
{
    self->capacity = 4;
    self->array = static_cast<int *>(malloc(sizeof(int) * self->capacity));
    self->indexHead = 0;
    self->indexLast = 0;
    self->size = 0;
};
void realloc(circularArray *self)
{
    self->capacity = self->capacity * 2;
    self->array = static_cast<int *>(realloc(self->array, sizeof(int) * self->capacity));
    if (self->indexHead != 0)
    {
        int bufFromHeaad[self->size];
        int bufFromLast[self->size];
        for (int i = self->indexHead, k = 0; i < self->size; i++, k++)
        {
            bufFromHeaad[k] = self->array[i];
        }
       

        for (int i = 0; i <= self->indexLast; i++)
        {
            bufFromLast[i] = self->array[i];
        }
        for (int i = 0; i < self->size-self->indexHead; i++)
        {
            self->array[i]=bufFromHeaad[i];
        }
        for (int i = self->size-self->indexHead,k=0;i < self->size; i++,k++)
        {
            self->array[i]=bufFromLast[k];

        }
    }
    self->indexHead=0;
    self->indexLast=self->size-1;
}
void deinit(circularArray *self)
{
    free(self->array);
}
void insert(int value, circularArray *self)
{
    if (self->capacity <= self->size)
        realloc(self);

    if (self->size > 0)
    {
        self->indexLast = (self->indexLast + 1) % self->capacity;
    }
    self->size += 1;
    self->array[self->indexLast] = value;
}
void pop(circularArray *self)
{
    if (self->size == 0)
        return;
    self->indexHead = (self->indexHead + 1) % self->capacity;
    if (self->size == 1)
    {
        self->indexLast = self->indexHead;
        self->size=0;
    }
    else
    {
        self->size -= 1;
    }
    
        
   
}
void print(circularArray *self)
{
    if(self->size==0)
    return;

    int i = self->indexHead;
    while (i != self->indexLast)
    {
        printf("|%i| ", self->array[i]);
        i = (i + 1) % self->capacity;
    }
    /*  if(self->size!=0) */
   
    
    puts("");
}

int main(int argc, char const *argv[])
{
    circularArray arr;
    init(&arr);
    /* printf("%i - capacity\n", arr.capacity);
    printf("%i - size\n", arr.size); */
    insert(5, &arr);
    insert(3, &arr);
    insert(6, &arr);
    insert(10, &arr);
    insert(12, &arr);
    insert(5, &arr);
    insert(3, &arr);
    insert(6, &arr);
    print(&arr);
    
    

    pop(&arr);
    pop(&arr);
    pop(&arr);
    pop(&arr);
    print(&arr);
     

  
    


     insert(10, &arr);
   
    print(&arr);
    

     insert(12, &arr);
     insert(5, &arr);
     insert(3, &arr);
     insert(6, &arr); 
    
     print(&arr);
     pop(&arr);
    pop(&arr);
    pop(&arr);
    pop(&arr);
    
     print(&arr);
     insert(1, &arr);
     insert(2, &arr);
     insert(3, &arr);
     insert(4, &arr);
    insert(5, &arr);
     insert(6, &arr);
     insert(7, &arr);
     insert(8, &arr);
     insert(69, &arr);
     insert(79, &arr);
     insert(89, &arr);
          insert(99, &arr);
     
     print(&arr);
      pop(&arr);
    pop(&arr);
    pop(&arr);
     pop(&arr);
    pop(&arr);
    pop(&arr);
     pop(&arr);
    pop(&arr);
    pop(&arr);
     pop(&arr);
    pop(&arr);
    pop(&arr);
    
    print(&arr);
    pop(&arr);
     pop(&arr);
    pop(&arr);
    pop(&arr);
    
print(&arr);
printf("+%i+\n",arr.size);
  pop(&arr);
  printf("+%i+\n",arr.size);
    pop(&arr);
   /*  print(&arr); */
    printf("|%i|\n",arr.size);


    deinit(&arr);

    return 0;
}
