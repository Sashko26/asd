#include <stdio.h>
#include <stdlib.h>

#include "queue.h"
#include <stdbool.h>


void queue_init(Queue *self)
{
    self->capacity = 4;
    self->array =malloc(sizeof(int) * self->capacity);
    self->indexHead = 0;
    self->indexLast = 0;
    self->size = 0;
};
void Queue_realloc(Queue *self)
{
    self->capacity = self->capacity * 2;
    self->array =realloc(self->array, sizeof(int) * self->capacity);
    if (self->indexHead != 0)
    {
        int bufFromHeaad[self->size];
        int bufFromLast[self->size];
        for (int i = self->indexHead, k = 0; i < self->size; i++, k++)
        {
            bufFromHeaad[k] = self->array[i];
        }
       

        for (int i = 0; i <= self->indexLast; i++)
        {
            bufFromLast[i] = self->array[i];
        }
        for (int i = 0; i < self->size-self->indexHead; i++)
        {
            self->array[i]=bufFromHeaad[i];
        }
        for (int i = self->size-self->indexHead,k=0;i < self->size; i++,k++)
        {
            self->array[i]=bufFromLast[k];

        }
    }
    self->indexHead=0;
    self->indexLast=self->size-1;
}
void queue_deinit(Queue *self)
{
    free(self->array);
}
void queue_insert(int value, Queue *self)
{
    if (self->capacity <= self->size)
        Queue_realloc(self);

    if (self->size > 0)
    {
        self->indexLast = (self->indexLast + 1) % self->capacity;
    }
    self->size += 1;
    self->array[self->indexLast] = value;
}
int queue_pop(Queue *self)
{
    if (self->size == 0)
        return -1;
    self->indexHead = (self->indexHead + 1) % self->capacity;
    if (self->size == 1)
    {
        int value=self->array[self->indexLast];
        self->indexLast = self->indexHead;
        self->size=0;
        return value;
    }
    else
    {
        int value=self->array[self->indexLast];
        self->size -= 1;
        return value;
    }
}
void queue_print(Queue *self)
{
    if(self->size==0)
    return;

    int i = self->indexHead;
    while (i != self->indexLast)
    {
        printf("|%i| ", self->array[i]);
        i = (i + 1) % self->capacity;
    }    
     printf("|%i| ", self->array[self->indexLast]);
    puts("");
}

bool queue_isEmpty(Queue *self)
{
    if(self->size==0)
    return true;
    return false;
}
