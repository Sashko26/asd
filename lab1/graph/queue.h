#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
typedef struct Queue
{
    int *array;
    int capacity;
    int size;
    int indexHead;
    int indexLast;
} Queue;

void queue_init(Queue *self);
void queue_realloc(Queue *self);
void queue_deinit(Queue *self);
void queue_insert(int value, Queue *self);
int queue_pop(Queue *self);
void queue_print(Queue *self);
bool queue_isEmpty(Queue *self);