// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <time.h>
#include <assert.h>
#include <string.h>
#define N 10
typedef struct SLNode
{
    int data;
    struct SLNode *next;

} SLNode;
//prototype of function
void merge(SLNode *head, int begin, int mid, int end);
void mergeSort(SLNode *head, int begin, int end);
SLNode *insertByIndex(SLNode *head, int index, int element);
int getValue(SLNode *head, int index);
SLNode *addSLNode(SLNode *head, SLNode *node);
void printSLList(SLNode *list);
SLNode *createSLNode(int data);
void algoritmVstavka(SLNode *head, int end, int begin);
SLNode *TryTofreeHeapfromSLlist(SLNode *SLlist);
void mainTest();

//program
int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-t") == 0)
    {
        mainTest();
    }
    else
    {
        srand(time(NULL));
        SLNode *head = createSLNode(rand() % 256 - 128);
        for (int i = 1; i < N; i++)
        {
            head = addSLNode(head, createSLNode(rand() % 256 - 128));
        }
        SLNode *bufNode = head;
        printSLList(bufNode);
        puts("");
        int begin, end;
        begin = 0;
        end = N - 1;
        mergeSort(head, begin, end);
        bufNode = head;
        printSLList(bufNode);
        head = TryTofreeHeapfromSLlist(head);

        puts("");
    }

    return 0;
}
//realization of function

//insert by index
SLNode *insertByIndex(SLNode *head, int index, int element)
{
    SLNode *bufNode = head;
    if (head == NULL)
    {
        fprintf(stderr, "error,this list is empty\n");
        return NULL;
    }
    else if (index == 0)
    {
        head->data = element;
        return head;
    }
    else
    {
        int i = 0;
        while (i != index)
        {
            bufNode = bufNode->next;
            i++;
        }
        bufNode->data = element;
    }

    return head;
}
//функція переходу до елементу за вказаним індексом
int getValue(SLNode *head, int index)
{

    SLNode *bufNode = head;
    if (head == NULL)
    {
        fprintf(stderr, "error,this list is empty\n");
        return 1;
    }
    else if (index == 0)
    {
        return head->data;
    }
    else if (head->next == NULL)
    {
        return head->data;
    }
    else
    {
        int i = 0;
        while (i != index)
        {
            bufNode = bufNode->next;
            i++;
        }
    }
    return bufNode->data;
}
// добавлення нового елемента до односпрямованого списку
SLNode *addSLNode(SLNode *head, SLNode *node)
{
    node->next = head;
    head = node;
    return head;
}
//функція виведення односпрямовваного списку в консоль
void printSLList(SLNode *list)
{
    while (list)
    {
        printf("|%i| ", list->data);
        list = list->next;
    }
    puts("");
}
//cтворення елементу списку
SLNode *createSLNode(int data)
{
    SLNode *head = malloc(sizeof(SLNode));
    head->data = data;
    head->next = NULL;
    return head;
}
//breakdown
void mergeSort(SLNode *head, int begin, int end)
{
    if (end - begin <= 3)
    {
        algoritmVstavka(head, end, begin);
        return;
    }

    else
    {
        int mid = begin + (end - begin) / 2;
        mergeSort(head, begin, mid);
        mergeSort(head, mid + 1, end);
        merge(head, begin, mid, end);
    }
}
//merging
void merge(SLNode *head, int begin, int mid, int end)
{
    int j, k, i, h, indexPos, sortArr[N], posArr[N];
    h = begin;
    j = mid + 1;
    indexPos = 0;
    i = begin;
    while (h <= mid && j <= end)
    {

        if (getValue(head, h) < 0 && getValue(head, j) < 0)
        {
            if (abs(getValue(head, h)) <= abs(getValue(head, j)))
            {
                sortArr[i] = getValue(head, j);
                j++;
            }
            else
            {
                sortArr[i] = getValue(head, h);
                h++;
            }
            i++;
        }

        else if (getValue(head, h) >= 0 && getValue(head, j) < 0)
        {
            sortArr[i] = getValue(head, j);
            posArr[indexPos] = getValue(head, h);
            h++;
            j++;
            indexPos++;
            i++;
        }
        else if (getValue(head, j) >= 0 && getValue(head, h) < 0)
        {
            sortArr[i] = getValue(head, h);
            h++;
            i++;
        }
        else if (getValue(head, j) >= 0 && getValue(head, h) >= 0)
        {
            posArr[indexPos] = getValue(head, h);
            indexPos++;

            h++;
        }
    }

    if (h > mid)
    {
        for (k = j; k <= end; k++)
        {
            if (getValue(head, k) < 0)
            {
                sortArr[i] = getValue(head, k);
                i++;
            }
            else
            {
                posArr[indexPos] = getValue(head, k);
                indexPos++;
            }
        }
    }
    else
    {
        for (k = h; k <= mid; k++)
        {
            if (getValue(head, k) < 0)
            {
                sortArr[i] = getValue(head, k);
                i++;
            }
            else
            {
                posArr[indexPos] = getValue(head, k);
                indexPos++;
            }
        }
    }
    for (k = begin; k < i; k++)
    {
        insertByIndex(head, k, sortArr[k]);
    }
    for (int bufIndexPos = 0, k = i; k <= end; k++, bufIndexPos++)
    {
        insertByIndex(head, k, posArr[bufIndexPos]);
    }
}
SLNode *TryTofreeHeapfromSLlist(SLNode *SLlist)
{
    while (SLlist != NULL)
    {
        SLNode *helperSLNode = SLlist;
        SLlist = SLlist->next;
        free(helperSLNode);
    }

    return SLlist;
}

// застосування алгоритму вставкою при малих розмірах підмасивів
void algoritmVstavka(SLNode *head, int end, int begin)
{
    if (end != begin)
    {
        for (int i = begin + 1; i <= end; i++)
        {
            int j = i;
            while (j > begin)
            {
                if (getValue(head, j) < 0 && getValue(head, j - 1) >= 0)
                {
                    int buf = getValue(head, j);
                    insertByIndex(head, j, getValue(head, j - 1));
                    insertByIndex(head, j - 1, buf);
                }
                else if (getValue(head, j) < 0 && getValue(head, j - 1) < 0)
                {
                    if (abs(getValue(head, j)) > abs(getValue(head, j - 1)))
                    {
                        int buf = getValue(head, j);
                        insertByIndex(head, j, getValue(head, j - 1));
                        insertByIndex(head, j - 1, buf);
                    }
                }
                j--;
            }
        }
    }
}
void mainTest()
{

    SLNode *list = createSLNode(-24);
    list = addSLNode(list, createSLNode(17));
    list = addSLNode(list, createSLNode(-45));
    list = addSLNode(list, createSLNode(98));
    list = addSLNode(list, createSLNode(1));
    list = addSLNode(list, createSLNode(-67));
    list = addSLNode(list, createSLNode(-43));
    algoritmVstavka(list, 6, 0);
    int arr[7] = {-67, -45, -43, -24, 1, 98, 17};
    SLNode *bufNode = list;
    for (int i = 0; i < 7; i++)
    {
        assert(arr[i] == bufNode->data);
        bufNode = bufNode->next;
    }
    list = TryTofreeHeapfromSLlist(list);
    assert(list == NULL);
    // перевірка працездатності функції mergeSort
    SLNode *head = createSLNode(-23);
    head = addSLNode(head, createSLNode(17));
    head = addSLNode(head, createSLNode(-48));
    head = addSLNode(head, createSLNode(15));
    head = addSLNode(head, createSLNode(0));
    head = addSLNode(head, createSLNode(-56));
    head = addSLNode(head, createSLNode(-48));
    head = addSLNode(head, createSLNode(52));
    mergeSort(head, 0, 7);
    int array[8] = {-56, -48, -48, -23, 52, 0, 15, 17};
    bufNode = head;
    for (int i = 0; i < 8; i++)
    {
        assert(array[i] == bufNode->data);
        bufNode = bufNode->next;
    }
    head = TryTofreeHeapfromSLlist(head);
    assert(head == NULL);
    puts("Success!");
}