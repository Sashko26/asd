#include <iostream>
#include <string>
void merge(int* arr,int left,int right);
void mergeSort(int arr[],int left, int right);
int main(int argc, char const *argv[])
{
    int array[40];
    for(int i=0;i<40;i++)
    {
        array[i]=rand()%40;
        printf("|%i| ",array[i]);
    }
    puts("");
    mergeSort(array,0,39);
     for(int i=0;i<40;i++)
    {
         printf("|%i| ",array[i]);
    }
 return 0;
}
void merge(int* arr,int left,int right)
{
    int length=1000;
    int i=left;
    int j=(left+right)/2 + 1;
    int middle=(left+right)/2;
    int bufArr[length];
    int k=0;
    while(i<=middle && j<=right)
    {
        if(arr[i]<=arr[j])
        {
            bufArr[k]= arr[i]; i++; k++;
        }
        else if(arr[j]<arr[i])
        {
            bufArr[k]= arr[j]; j++; k++;
        }
    }
   while(i<=middle)
   {
       bufArr[k]=arr[i]; i++; k++;
   }
   while(j<=right)
   {
       bufArr[k]=arr[j]; j++; k++;
   }
   for(int i=0;i<k;i++)
   {
       arr[i+left]=bufArr[i];
   }
 
}
void mergeSort(int arr[],int left, int right)
{
    int buf=0;
    if(left<=right)
    {
        if(left-right==0)
        {
            if(arr[left] > arr[right])
            {
                buf = arr[left];
                arr[left]=arr[right];
                arr[right]=buf;
            }
        }
        else
        {   
            mergeSort(arr,left,(left+right)/2);
            mergeSort(arr,(left+right)/2+1,right);
            merge(arr,left,right);
        }
    }
}
