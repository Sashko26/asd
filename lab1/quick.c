// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

void quick_sort(int *arr, int size /* найбільший індекс в масиві */)
{
    int subSize = size;
    int begin = 0;
    int midPivot = arr[subSize / 2];
    int buf = 0;
    while (begin <= subSize)
    {
        while (arr[begin] < midPivot)
            begin++;
        while (arr[subSize] > midPivot)
            subSize--;
        if (begin <= subSize)
        {
            buf = arr[begin];
            arr[begin] = arr[subSize];
            arr[subSize] = buf;
            begin++;
            subSize--;
        }
    }
    if(subSize>0) quick_sort(arr,subSize);
    if(size>begin) quick_sort(arr+begin,size-begin);
}
int main()
{
    int size=20;
    int array[size];
    for (int i = 0; i < size; i++)
    {
        array[i] = rand() % 40;
        printf("|%i| ", array[i]);
    }
    quick_sort(array,size-1);
    puts("");
     for (int i = 0; i < size; i++)
    {
        printf("|%i| ", array[i]);
    }
    
    puts("");
    return 0;
}
