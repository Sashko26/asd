#include "shack.h"

void mainTest();

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-t") == 0)
    {
        mainTest();
    }
    else
    {
        char string[1000] = "\0";
        fgets(string, 1000, stdin);
        string[strlen(string)] = '\0';
        Stack *postFixString = ConvertStringToPostFix(string);
        Stack_print(postFixString);
        printf("Your result is my work:%i\n", CalculateFromInFix(postFixString));
        Stack_deinit(postFixString);
        free(postFixString);
    }
    return 0;
}

void mainTest()
{

    StackInt stack;
    StackInt_init(&stack);
    StackInt_push(&stack, 1);
    assert(StackInt_Peek(&stack) == 1);
    StackInt_push(&stack, 7);
    assert(StackInt_Peek(&stack) == 7);
    StackInt_push(&stack, 3);
    assert(StackInt_Peek(&stack) == 3);
    StackInt_pop(&stack);
    assert(StackInt_Peek(&stack) == 7);
    char string[1000] = "(2-1)*3 +7";
    Stack *postFixString = ConvertStringToPostFix(string);
    char string1[1000] = "((8-4)*4)-3";
    char string2[100] = "((12+12)-17*4-23/27+4)";
    char string3[100] = "((12/4+7*5-(12+4-43*2)))";
    Stack *postFixString3 = ConvertStringToPostFix(string3);
    Stack *postFixString2 = ConvertStringToPostFix(string2);
    Stack *postFixString1 = ConvertStringToPostFix(string1);
    assert(CalculateFromInFix(postFixString) == 10);
    assert(CalculateFromInFix(postFixString1) == 13);
    assert(CalculateFromInFix(postFixString2) == -40);
    assert(CalculateFromInFix(postFixString3) == 108);
    printf("success!\n");
    Stack_deinit(postFixString);
    free(postFixString);
    Stack_deinit(postFixString1);
    free(postFixString1);
    Stack_deinit(postFixString2);
    free(postFixString2);


    Stack_deinit(postFixString3);
    free(postFixString3);
    StackInt_deinit(&stack);
}

