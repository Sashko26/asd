#pragma once
#include <stdlib.h> // Деякі додаткові функції
#include <stdio.h>  // Для друку в термінал
#include <math.h>   // Для математичних функцій
#include <stdlib.h> // Деякі додаткові функції
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <progbase/console.h>
#include <assert.h>

typedef struct Stack
{
    void **items;
    int length;
    int capacity;
    int front;

} Stack;
typedef struct StackFloat
{
    int *array;
    int length;
    int capacity;
    int front;

} StackInt;
void Stack_realloc(Stack *self, int newCapacity);
void Stack_init(Stack *self);
void *Stack_Peek(Stack *self);
int Stack_size(Stack *self);
int Stack_indexOfPeek(Stack *self);
char *Stack_getValueByindex(Stack *self, int index);
void Stack_pop(Stack *self);
void Stack_push(Stack *self, char *value);
void Stack_realloc(Stack *self, int newCapacity);
void Stack_deinit(Stack *self);
void Stack_print(Stack *self);

//My special function
Stack *ConvertStringToPostFix(char *string);
int CalculateFromInFix(Stack *self);

//stack int
void StackInt_init(StackInt *self);
void StackInt_deinit(StackInt *self);

void StackInt_push(StackInt *self, int value);
int StackInt_pop(StackInt *self);
void StackInt_realloc(StackInt *self, int newCapacity);

int StackInt_Peek(StackInt *self);
void StackInt_print(StackInt *self);

int StackInt_size(StackInt *self);
int Deque_getValueByindex(StackInt *self, int index);


