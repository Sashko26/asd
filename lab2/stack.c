// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>  // Для друку в термінал
#include <math.h>   // Для математичних функцій
#include <stdlib.h> // Деякі додаткові функції
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#include "shack.h"

// ініціалізація стеку
void Stack_init(Stack *self)
{
    self->length = 0;
    self->front = -1;
    self->capacity = 4;
    self->items = malloc(sizeof(void *) * self->capacity);
    if (self->items == NULL)
    {
        fprintf(stderr, "Allocation error");
        abort();
    }
}
// функція для отримання значення  вершини
void *Stack_Peek(Stack *self)
{
    if (Stack_size(self) == 0)
    {
        return NULL;
    }
    char *val = self->items[self->front + self->length - 1];
    return val;
}
int Stack_size(Stack *self)
{
    int val = self->length;
    return val;
}
int Stack_indexOfPeek(Stack *self)
{
    int val = self->front + self->length - 1;
    return val;
}

char *Stack_getValueByindex(Stack *self, int index)
{
    char *val = self->items[self->front + index];
    return val;
}
//видалення елементу зі стеку
void Stack_pop(Stack *self)
{
    if (self->length == 0)
    {
        fprintf(stderr, "stack is underflow");
        abort();
    }
    void *val = self->items[self->front + self->length - 1];
    free(val);
    self->length = self->length - 1;
}
// вставлення елементу стеку в кінець
void Stack_push(Stack *self, char *value)
{
    if (self->front + self->length == self->capacity)
    {
        int newCapacity = self->capacity * 2;
        Stack_realloc(self, newCapacity);
        self->length = self->length + 1;
        self->items[self->front + self->length - 1] = malloc(sizeof(char) * 20);
        strcpy((char *)self->items[self->front + self->length - 1], value);
        self->capacity = newCapacity;
    }
    else if (self->front == -1)
    {
        self->front = 0;
        self->length = 1;
        self->items[self->front] = malloc(sizeof(char) * 20);
        strcpy((char *)self->items[self->front], value);
    }
    else
    {
        self->length = self->length + 1;
        self->items[self->front + self->length - 1] = malloc(sizeof(char) * 20);
        strcpy((char *)self->items[self->front + self->length - 1], value);
    }
}

// перевиділення динамічної пам'яті
void Stack_realloc(Stack *self, int newCapacity)
{
    void *newMem = realloc(self->items, sizeof(void *) * newCapacity);
    if (newMem == NULL)
    {
        free(self->items);
        fprintf(stderr, "reallocation error");
        abort();
    }
    self->items = newMem;
    self->capacity = newCapacity;
}
// звільнення динамічної пам'яті від cтеку
static void Clear_stack(Stack *self)
{
    for (int i = 0; i < Stack_size(self); i++)
    {
        free(self->items[self->front + i]);
    }
}
void Stack_deinit(Stack *self)
{
    Clear_stack(self);
    free(self->items);
}
void Stack_print(Stack *self)
{
    if (self->front == -1)
    {
        fprintf(stderr, "Stack is empty!");
    }
    else
    {
        int beginOfprint = self->front;
        int endOfprint = self->front + self->length;
        for (int i = beginOfprint; i < endOfprint; i++)
        {
            printf("|%s|", (char *)self->items[i]);
        }
        puts("");
    }
}
Stack *ConvertStringToPostFix(char *string)
{
    int brackets = 0;
    Stack *postFixString = malloc(sizeof(Stack));
    Stack operator;
    Stack_init(&operator);
    Stack_init(postFixString);
    int i = 0;
    while (1)
    {
        if (isdigit(string[i]))
        {
            while (isdigit(string[i]))
            {
                char bufStringForNumber[20] = "\0";
                int bufStringIndex = 0;
                while (isdigit(string[i]))
                {
                    bufStringForNumber[bufStringIndex] = string[i];
                    i++;
                    bufStringIndex++;
                }
                bufStringForNumber[strlen(bufStringForNumber)] = '\0';
                Stack_push(postFixString, bufStringForNumber);
               

            }
        }
        else if (string[i] == '+' || string[i] == '-' || string[i] == '*' || string[i] == '/' || string[i] == '(' || string[i] == ')')
        {
            if (Stack_Peek(&operator) == NULL)
            {
                if (string[i] == '+')
                {
                    Stack_push(&operator, "+");
                    i++;
                }

                else if (string[i] == '-')
                {
                    Stack_push(&operator, "-");
                    i++;
                }
                else if (string[i] == '*')
                {
                    Stack_push(&operator, "*");
                    i++;
                }
                else if (string[i] == '/')
                {
                    Stack_push(&operator, "/");
                    i++;
                }
                else if (string[i] == '(')
                {
                    brackets++;
                    Stack_push(&operator, "(");
                    i++;
                   
                }
               
                else 
                {
                    printf("|%c|\n",string[i]);
                    fprintf(stderr, "not correct writing");
                    abort();
                }
            }
            else
            {
                if (string[i] == '(')
                {
                    brackets++;
                    Stack_push(&operator, "(");
                    i++;
                }
                else if (string[i] == ')')
                {
                    if (brackets > 0)
                    {
                        
                        while (strcmp((char *)Stack_Peek(&operator), "(") != 0)
                        {
                            Stack_push(postFixString, Stack_Peek(&operator));
                            Stack_pop(&operator);
                        }
                        Stack_pop(&operator);
                        i++;
                        brackets--;
                        
                    }
                    else
                    {
                        fprintf(stderr, "Not correct writing!");
                        abort();
                    }
                }
                else if (string[i] == '/')
                {
                    if (strcmp(Stack_Peek(&operator), "*") == 0 || strcmp(Stack_Peek(&operator), "/") == 0)
                    {
                        Stack_push(postFixString, Stack_Peek(&operator));
                        Stack_pop(&operator);
                        Stack_push(&operator, "/");
                        i++;
                    }
                    else
                    {
                        Stack_push(&operator, "/");
                        i++;
                    }
                }
                else if (string[i] == '*')
                {
                    if (strcmp(Stack_Peek(&operator), "/") == 0 || strcmp(Stack_Peek(&operator), "*") == 0)
                    {
                        Stack_push(postFixString, Stack_Peek(&operator));
                        Stack_pop(&operator);
                        Stack_push(&operator, "*");
                        i++;
                    }
                    else
                    {
                        Stack_push(&operator, "*");
                        i++;
                    }
                }

                else if (string[i] == '+')
                {
                    if (brackets == 0)
                    {
                        while (Stack_Peek(&operator) != NULL)
                        {
                            Stack_push(postFixString, Stack_Peek(&operator));
                            Stack_pop(&operator);
                        }
                        Stack_push(&operator, "+");
                        i++;
                    }
                    else
                    {
                        while (strcmp(Stack_Peek(&operator), "(") != 0)
                        {
                            Stack_push(postFixString, Stack_Peek(&operator));
                            Stack_pop(&operator);
                        }
                        Stack_push(&operator, "+");
                        i++;
                    }
                }
                else if (string[i] == '-')
                {
                    if (brackets == 0)
                    {
                        while (Stack_Peek(&operator) != NULL)
                        {
                            Stack_push(postFixString, Stack_Peek(&operator));
                            Stack_pop(&operator);
                        }
                        Stack_push(&operator, "-");
                        i++;
                    }
                    else
                    {
                        while (strcmp(Stack_Peek(&operator), "(") != 0)
                        {
                            Stack_push(postFixString, Stack_Peek(&operator));
                            Stack_pop(&operator);
                        }
                        Stack_push(&operator, "-");
                        i++;
                    }
                }
            }
        }
        else
        {
            if (Stack_Peek(&operator) != NULL && i == strlen(string))
            {

                while (Stack_Peek(&operator) != NULL)
                {
                    Stack_push(postFixString, Stack_Peek(&operator));
                    Stack_pop(&operator);
                }
                if (brackets > 0)
                {

                    fprintf(stderr, "Ви дужки не закрили!\n");
                    abort();
                }

                break;
            }
            else if (isspace(string[i]))
            {
                i++;
            }
            else if (string[i] == '\0')
            {
                break;
            }
            else
            {   
                
                fprintf(stderr, "Не коректна форма запису числового виразу!\n");
                abort();
            }
        }
    }
    Stack_deinit(&operator);
    return postFixString;
}
int CalculateFromInFix(Stack *self)
{
    int result = 0;
    StackInt StackInt;
    StackInt_init(&StackInt);
    for (int i = 0; i < self->length; i++)
    {
        if (atoi(self->items[i]) != 0)
        {
            StackInt_push(&StackInt, atoi(self->items[i]));
        }
        else if (strcmp(self->items[i], "+") == 0 || strcmp(self->items[i], "/") == 0 || strcmp(self->items[i], "-") == 0 || strcmp(self->items[i], "*") == 0)
        {
            if (strcmp(self->items[i], "+") == 0)
            {
                int first, second;
                first = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                second = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                StackInt_push(&StackInt, first + second);
            }
            else if (strcmp(self->items[i], "-") == 0)
            {
                int first, second;
                first = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                second = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                StackInt_push(&StackInt, second - first);
            }
            else if (strcmp(self->items[i], "*") == 0)
            {
                int first, second;
                first = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                second = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                StackInt_push(&StackInt, second * first);
            }
            else if (strcmp(self->items[i], "/") == 0)
            {
                int first, second;
                first = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                second = StackInt_Peek(&StackInt);
                StackInt_pop(&StackInt);
                StackInt_push(&StackInt, second / first);
            }
        }
        else if (strcmp(self->items[i], "0") == 0)
        {
            StackInt_push(&StackInt, atof(self->items[i]));
            if (strcmp(self->items[i + 1], "/") == 0)
            {
                fprintf(stderr, "На нуль ділити не можна!\n");
                abort();
            }
        }
    }
    result = StackInt_Peek(&StackInt);
    StackInt_deinit(&StackInt);
    return result;
}

//int Stack
void StackInt_init(StackInt *self)
{
    self->length = 0;
    self->front = -1;
    self->capacity = 4;
    self->array = malloc(sizeof(int) * self->capacity);
    if (self->array == NULL)
    {
        fprintf(stderr, "Allocation error");
        abort();
    }
}
// функція для отримання значення вершини
int StackInt_Peek(StackInt *self)
{
    int val = self->array[self->front + self->length - 1];
    return val;
}
int StackInt_size(StackInt *self)
{
    int val = self->length;
    return val;
}

//видалення елементу
int StackInt_pop(StackInt *self)
{
    if (self->length == 0)
    {
        fprintf(stderr, "stack is underflow");
        abort();
    }
    int val = self->array[self->front + self->length - 1];
    self->length = self->length - 1;
    return val;
}
// вставлення елементу
void StackInt_push(StackInt *self, int value)
{
    if (self->front + self->length == self->capacity)
    {
        int newCapacity = self->capacity * 2;
        StackInt_realloc(self, newCapacity);
        self->length = self->length + 1;
        self->array[self->front + self->length - 1] = value;
        self->capacity = newCapacity;
    }
    else if (self->front == -1)
    {
        self->front = 0;
        self->length = 1;
        self->array[self->front] = value;
    }
    else
    {
        self->length = self->length + 1;
        self->array[self->front + self->length - 1] = value;
    }
}
// перевиділення динамічної пам'яті
void StackInt_realloc(StackInt *self, int newCapacity)
{
    int *newMem = realloc(self->array, sizeof(int) * newCapacity);
    if (newMem == NULL)
    {
        free(self->array);
        fprintf(stderr, "reallocation error");
        abort();
    }
    self->array = newMem;
    self->capacity = newCapacity;
}
// звільнення динамічної пам'яті від стеку
void StackInt_deinit(StackInt *self)
{
    free(self->array);
}
void StackInt_print(StackInt *self)
{
    if (self->front == -1)
    {
        fprintf(stderr, "Stack is empty!");
    }
    else
    {
        int beginOfprint = self->front;
        int endOfprint = self->front + self->length;
        for (int i = beginOfprint; i < endOfprint; i++)
        {
            printf("|%i| ", self->array[i]);
        }
        puts("");
    }
}

