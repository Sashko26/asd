#include "hashtable.hpp"
static int hashCodeInt(int reservationNumber)
{
    return reservationNumber;
}
static int hashCodeString(char *lastName)
{
    int hash = 0;
    int powerOfAlphabet = 5;

    for (int i = 0, j = strlen(lastName) - 1; i < strlen(lastName) && j >= 0; i++, j--)
    {
        int numberOfsymbol = 0;
        if (isupper(lastName[i]))
        {
            numberOfsymbol = lastName[i] - 64;
        }
        else if (islower(lastName[i]))
        {
            numberOfsymbol = lastName[i] - 96;
        }

        int hashOfCurrentSymbol = numberOfsymbol * pow(powerOfAlphabet, j);
        hash = hash + hashOfCurrentSymbol;
    }
    return hash;
}
 insertForAdd(hashTable *self, flightCodeInstance *flightInstance)
{
    int finalHashCode = hashCodeString(flightInstance->flightCode) % self->list->capacity;
    if (self->list->items[finalHashCode] == NULL)
    {
        self->list->items[finalHashCode] = flightInstance;
    }
    else
    {
        while (self->list->items[finalHashCode] != NULL)
        {
            finalHashCode = (finalHashCode + 1) % self->list->capacity;
        }
        self->list->items[finalHashCode] = flightInstance;
    }
    return self;
}
void sameFlightPassengers(hashTable *hash, char *flightCode)
{
    hashTable *addHashTable = static_cast<hashTable *>(malloc(sizeof(hashTable)));
    addHashTable->list = static_cast<List *>(malloc(sizeof(List)));
    addHashTable->list->capacity = 20;
    addHashTable->list->size = 0;
    addHashTable->list->items = static_cast<void **>(malloc(sizeof(void *) * addHashTable->list->capacity));
    for (int i = 0; i < addHashTable->list->capacity; i++)
    {
        addHashTable->list->items[i] = NULL;
    }
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-M")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-O")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-N")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-T")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-M-1")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-P")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-R")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-L-1")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-L")));
    addHashTable = insertForAdd(addHashTable, createInstanceForSameFlight(hash, const_cast<char *>("K-C")));

    for (int i = 0; i < addHashTable->list->capacity; i++)
    {
        if (addHashTable->list->items[i] != NULL)

        {
            if (strcmp(static_cast<flightCodeInstance *>(addHashTable->list->items[i])->flightCode, flightCode) == 0)
            {
                for (int j = 0; j < static_cast<flightCodeInstance *>(addHashTable->list->items[i])->size; j++)
                {
                    printf("%i-th passenger of flight |%s| |%i|\n", j, flightCode, static_cast<flightCodeInstance *>(addHashTable->list->items[i])->listPassengersOfflight[j]);
                }
            }
        }
    }

    for (int i = 0; i < addHashTable->list->capacity; i++)
    {
        if (addHashTable->list->items[i] != NULL)
        {
            free(addHashTable->list->items[i]);
        }
    }
    free(addHashTable->list->items);
    free(addHashTable->list);
    free(addHashTable);
}
flightCodeInstance *createInstanceForSameFlight(hashTable *hash, char *flightCode)
{
    flightCodeInstance *instance = static_cast<flightCodeInstance *>(malloc(sizeof(flightCodeInstance)));
    instance->capacity = 6;
    instance->size = 0;

    strcpy(instance->flightCode, flightCode);

    for (int i = 0; i < hash->list->capacity; i++)
    {
        if (hash->list->items[i] != NULL)
        {
            if (strcmp(static_cast<keyValue *>(hash->list->items[i])->value->flightCode, flightCode) == 0)
            {
                instance = addNewReservationCode(instance, static_cast<keyValue *>(hash->list->items[i])->key->reservationNumber);
                cout << static_cast<keyValue *>(hash->list->items[i])->key->reservationNumber << endl;
            }
        }
    };
    return instance;
}
flightCodeInstance *addNewReservationCode(flightCodeInstance *self, int reservationNumber)
{

    if (self->size < self->capacity)
    {
        self->listPassengersOfflight[self->size] = reservationNumber;
        self->size = self->size + 1;
    }

    return self;
}

int hashCode(key *key)
{
    int hash = hashCodeString(key->last_name) + hashCodeInt(key->reservationNumber);
    return hash;
}

flight *createArrayOfFlight()
{
    flight *leastOfFlights = static_cast<flight *>(malloc(sizeof(flight) * 10));
    char matrixOfseats[6][4] = {"A-1", "B-1", "C-1", "A-2", "B-2", "C-2"};
    strcpy(leastOfFlights[0].flightCode, "K-M");
    strcpy(leastOfFlights[0].flightDirection, "Kiev-Madrid");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[0].seats[i].reserved = false;
        strcpy(leastOfFlights[0].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[1].flightCode, "K-O");
    strcpy(leastOfFlights[1].flightDirection, "Kiev-Odessa");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[1].seats[i].reserved = false;
        strcpy(leastOfFlights[1].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[2].flightCode, "K-N");
    strcpy(leastOfFlights[2].flightDirection, "Kiev-New York");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[2].seats[i].reserved = false;
        strcpy(leastOfFlights[2].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[3].flightCode, "K-T");
    strcpy(leastOfFlights[3].flightDirection, "Kiev-Tokio");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[3].seats[i].reserved = false;
        strcpy(leastOfFlights[3].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[4].flightCode, "K-M-1");
    strcpy(leastOfFlights[4].flightDirection, "Kiev-Marseille");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[4].seats[i].reserved = false;
        strcpy(leastOfFlights[4].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[5].flightCode, "K-P");
    strcpy(leastOfFlights[5].flightDirection, "Kiev-Paris");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[5].seats[i].reserved = false;
        strcpy(leastOfFlights[5].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[6].flightCode, "K-R");
    strcpy(leastOfFlights[6].flightDirection, "Kiev-Rome");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[6].seats[i].reserved = false;
        strcpy(leastOfFlights[6].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[7].flightCode, "K-L");
    strcpy(leastOfFlights[7].flightDirection, "Kiev-Lviv");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[7].seats[i].reserved = false;
        strcpy(leastOfFlights[7].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[8].flightCode, "K-L-1");
    strcpy(leastOfFlights[8].flightDirection, "Kiev-Los Angeles");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[8].seats[i].reserved = false;
        strcpy(leastOfFlights[8].seats[i].seat, matrixOfseats[i]);
    }

    strcpy(leastOfFlights[9].flightCode, "K-C");
    strcpy(leastOfFlights[9].flightDirection, "Kiev-Chicago");
    for (int i = 0; i < 6; i++)
    {
        leastOfFlights[9].seats[i].reserved = false;
        strcpy(leastOfFlights[9].seats[i].seat, matrixOfseats[i]);
    }
    return leastOfFlights;
}

hashTable *alloc()
{
    hashTable *hashtable = static_cast<hashTable *>(malloc(sizeof(hashTable)));
    hashtable->list = List_alloc();
    hashtable->loadness = 0;
    return hashtable;
}
void hashTable_free(hashTable *self)
{
    for (int i = 0; i < self->list->capacity; i++)
    {
        if (self->list->items[i] != NULL)
        {
            keyValue_clear(static_cast<keyValue *>(self->list->items[i]));
        }
    }
    List_free(self->list);
    free(self);
}

hashTable *rehashing(hashTable *hash)
{
    hashTable *newHash = static_cast<hashTable *>(malloc(sizeof(hashTable)));
    newHash->list = static_cast<List *>(malloc(sizeof(List)));
    newHash->list->capacity = hash->list->capacity * 2;
    newHash->list->items = static_cast<void **>(malloc(sizeof(void *) * newHash->list->capacity));
    newHash->list->size = hash->list->size;

    float size = newHash->list->size;
    float capacity = newHash->list->capacity;
    newHash->loadness = size / capacity;
    for (int i = 0; i < newHash->list->capacity; i++)
    {
        newHash->list->items[i] = NULL;
    }
    for (int i = 0; i < hash->list->capacity; i++)
    {
        if (hash->list->items[i] != NULL)
        {
            int finalHashCode = hashCode(static_cast<keyValue *>(hash->list->items[i])->key) % (newHash->list->capacity);
            if (newHash->list->items[finalHashCode] == NULL)
            {
                newHash->list->items[finalHashCode] = hash->list->items[i];
                printf("|%i|\n", finalHashCode);
                printf("|%s|\n", static_cast<keyValue *>(newHash->list->items[finalHashCode])->key->last_name);
                printf("переміщення елементу зі старої таблиці у збільшену\n");
            }
            else
            {
                while (newHash->list->items[finalHashCode] != NULL)
                {
                    finalHashCode = (finalHashCode + 1) % newHash->list->capacity;
                }
                newHash->list->items[finalHashCode] = hash->list->items[i];
                printf("|%i|\n", finalHashCode);
                printf("|%s|\n", static_cast<keyValue *>(newHash->list->items[finalHashCode])->key->last_name);
                printf("переміщення елементу зі старої таблиці у збільшену\n");
            }
        }
    }

    List_free(hash->list);
    free(hash);
    hash = newHash;
    return hash;
}
value *find(hashTable *self, key *key)
{
    for (int i = 0; i < self->list->capacity; i++)
    {
        if (self->list->items[i] != NULL)
        {
            if (key->reservationNumber == static_cast<keyValue *>(self->list->items[i])->key->reservationNumber)
            {
                return static_cast<keyValue *>(self->list->items[i])->value;
            }
        }
    }
    cout << "У таблиці не має екземпляру із заданим ключем!" << endl;
    return NULL;
}
int removeEl(hashTable *self, key *key)
{
    for (int i = 0; i < self->list->capacity; i++)
    {
        if (self->list->items[i] != NULL)
        {
            if (key->reservationNumber == static_cast<keyValue *>(self->list->items[i])->key->reservationNumber)
            {
                static_cast<keyValue *>(self->list->items[i])->value->deleted = true;
                self->list->size = self->list->size - 1;
                return 0;
            }
        }
    }
    cout << "У таблиці не було екземплеру з відповідним ключем!" << endl;
    return 1;
}
hashTable *insert(hashTable *self, keyValue *instance)
{

    if (self->loadness >= 0.5)
    {
        printf("realloc!\n");
        self = rehashing(self);
    }
    int finalHashCode = hashCode(instance->key) % (self->list->capacity);

    if (self->list->items[finalHashCode] == NULL)
    {
        printf("вставка на пусте місце!\n");
        printf("finalHashCode: %i\n", finalHashCode);
        self->list->items[finalHashCode] = instance;
        self->list->size++;
        float size = self->list->size;
        float capacity = self->list->capacity;
        self->loadness = size / capacity;
       /*  printf("|%f|\n", self->loadness); */
    }
    else if (self->list->items[finalHashCode] != NULL)

    {
        if (static_cast<keyValue *>(self->list->items[finalHashCode])->value->deleted == true)
        {
            keyValue_clear(static_cast<keyValue *>(self->list->items[finalHashCode]));
            self->list->items[finalHashCode] = instance;
            puts("push new instance in deleted instance! ");
            self->list->size++;
        }
        else
        {

            printf("|%i|\n", finalHashCode);
            printf("collision!!!!!!\n");
            bool insertForCollision = true;
            finalHashCode = (finalHashCode + 1) % self->list->capacity;
            while (insertForCollision)
            {
                if (self->list->items[finalHashCode] == NULL)
                {
                    insertForCollision = false;
                }
                else if (self->list->items[finalHashCode] != NULL)
                {
                    if (static_cast<keyValue *>(self->list->items[finalHashCode])->value->deleted == false)
                    {
                        finalHashCode = (finalHashCode + 1) % self->list->capacity;
                    }
                    else if (static_cast<keyValue *>(self->list->items[finalHashCode])->value->deleted == true)
                    {
                        insertForCollision = false;
                    }
                }
            }
            if (self->list->items[finalHashCode] == NULL)
            {

                self->list->items[finalHashCode] = instance;
                self->list->size++;
                float size = self->list->size;
                float capacity = self->list->capacity;
                self->loadness = size / capacity;
            }
            else if (static_cast<keyValue *>(self->list->items[finalHashCode])->value->deleted == true)
            {
                keyValue_clear(static_cast<keyValue *>(self->list->items[finalHashCode]));
                self->list->items[finalHashCode] = instance;
                self->list->size = self->list->size + 1;
                puts("push new instance in deleted instance! after collision");
            }
        }
    }
    return self;
}