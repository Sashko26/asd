#include "list.hpp"
#include "instance.hpp"
struct seat
{
    bool reserved;
    char seat[10];
};
typedef struct seats seats;
typedef struct flight flight;
struct flight
{
    char flightCode[100];
    char flightDirection[100];
    seat seats[6];
};
typedef struct flight flight;
flight *createArrayOfFlight();
//hashTable

struct hashTable
{
    List *list;
    float loadness;
};
typedef struct hashTable hashTable;
hashTable *alloc();
void hashTable_free(hashTable *self);

hashTable *insert(hashTable *self, keyValue *instance);
int removeEl(hashTable *self, key *key);
value *find(hashTable *self, key *key);
int hashCode(key *key);
static int getHash(hashTable *self, key *key);
hashTable *rehashing(hashTable *hash);
void sameFlightPassengers(hashTable *hash, char *flightCode);

///for additional function
struct flightCodeInstance
{
    char flightCode[100];
    int listPassengersOfflight[6];
    int size;
    int capacity;
};



flightCodeInstance *addNewReservationCode(flightCodeInstance *self, int reservationNumber);
hashTable* insertForAdd(hashTable *self, flightCodeInstance *flightInstance);
flightCodeInstance *createInstanceForSameFlight(hashTable *hash,char *flightCode);
