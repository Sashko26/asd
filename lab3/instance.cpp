#include "instance.hpp"
keyValue *createNewinstance(key *Key, value *Value)
{
    keyValue *reservTicket;
    reservTicket = new keyValue;
    reservTicket->key = Key;
    reservTicket->value = Value;
    return reservTicket;
}
key *createNewkey(int reservationNumber, char *last_name)
{
    key *Key = new key;
    Key->reservationNumber = reservationNumber;
    Key->last_name = new char[100];
    strcpy(Key->last_name, last_name);
    return Key;
}
value *createValue(const char *passportID, const char *flightCode, const char *seat, int boolean)
{
    value *Value = new value;
    Value->passportID = new char[100];
    Value->flightCode = new char[100];
    Value->seat = new char[100];
    Value->priorityBoarding = boolean;
    strcpy(Value->passportID, passportID);
    strcpy(Value->flightCode, flightCode);
    strcpy(Value->seat, seat);
    return Value;
}
void Key_clear(key *self)
{
    delete[] self->last_name;
    delete self;
}
void Value_clear(value *self)
{
    delete[] self->passportID;
    delete[] self->flightCode;
    delete[] self->seat;
    delete self;
}
void keyValue_clear(keyValue *self)
{
    Key_clear(self->key);
    Value_clear(self->value);
    delete self;
}
void print_key(key *self)
{
    cout << self->last_name << endl;
    cout << self->reservationNumber << endl;
}
void print_value(value *self)
{
    cout << self->passportID << endl;
    cout << self->flightCode << endl;
    cout << self->seat << endl;
    cout << self->priorityBoarding << endl;
}
void print_informationOfOneReservation(keyValue *self)
{
    cout << "key:" << endl;
    print_key(self->key);
    cout << endl;
    cout << "value:" << endl;
    print_value(self->value);
    cout << endl;
}