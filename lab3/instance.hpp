#pragma once
#include <string.h>
#include <iostream>
#include <cassert>
#include <cstdlib>
using namespace std;
struct value
{
    char *passportID;
    char *flightCode;
    char *seat;
    int priorityBoarding;
    bool deleted=false;
};
typedef struct value value;
struct key
{
    int reservationNumber;
    char *last_name;
};
typedef struct key key;

struct keyValue
{
    struct key *key;
    struct value *value;
};
typedef struct keyValue keyValue;
keyValue *createNewinstance(key *Key, value *Value);
key *createNewkey(int reservationNumber, char *last_name);
value *createValue(const char *passportID, const char *flightCode, const char *seat, int boolean);
void Key_clear(key *self);
void Value_clear(value *self);
void keyValue_clear(keyValue* self);



void print_informationOfOneReservation(keyValue *self);
void print_value(value *self);
void print_key(key *self);

