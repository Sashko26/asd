#include "list.hpp"



void List_realloc(List *self)
{
    int newCapacity = (int)self->capacity * 2;
    self->items = static_cast<void **>(realloc(self->items, sizeof(void *) * newCapacity));
    self->capacity = newCapacity;
    if (self->items == NULL)
    {
        fprintf(stderr, "пам'яті немає!\n");
        abort();
    }
}
// виділення динамічної пам'яті для  списку і його ініціалізація
List *List_alloc(void)
{
    List *self = static_cast<List *>(malloc(sizeof(List)));

    self->capacity = 4;
    self->size = 0;
    self->items = static_cast<void **>(malloc(sizeof(void *) * self->capacity));
    if (self->items == NULL)
    {
        fprintf(stderr, "пам'яті немає!\n");
        abort();
    }
    for (int i = 0; i < self->capacity; i++)
    {
        self->items[i] = NULL;
    }

    return self;
}
// звілнення динамічно виділеної пам'яті під items і під структуру
void List_free(List *self)
{
    free(self->items);
    free(self);
}







