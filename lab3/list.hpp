#pragma once
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <math.h>

typedef void * T;

struct  __List
{
    void ** items;
    size_t capacity;
    size_t size;
};

typedef struct __List List;

List *  List_alloc    (void);
void    List_free     (List * self);
void List_realloc(List *self);


