#include "AVLtree.h"
int height(node* root)
{
    return root?root->height:(-1);
}

int bfactor(node* root)
{
    return height(root->left)-height(root->right);
}

void fixheight(node* root)
{
    int hl = height(root->left);
    int hr = height(root->right);
    root->height = (hl>hr?hl:hr)+1;
}

node* rightRotate(node* root) // правий поворот навколо root
{
    node* l = root->left;
    root->left = l->right;
    l->right = root;
    fixheight(root);
    fixheight(l);
    return l;
}

node* leftRotate(node* root) // лівий поворот навколо root
{
    node* r = root->right;
    root->right = r->left;
    r->left = root;
    fixheight(root);
    fixheight(r);
    return r;
}

node* balancing(node* root) // балансування вузла root
{
    fixheight(root);
    if( bfactor(root)==-2 )
    {
        if( bfactor(root->right) > 0 )
        {
          root->right = rightRotate(root->right);
          cout<<"права-ліва незбалансованість"<<endl;
          return leftRotate(root);
        }
            cout<<"права незбалансованість"<<endl;
           return leftRotate(root);

    }
    if( bfactor(root)==2 )
    {
        if( bfactor(root->left) < 0)
        {
             root->left = leftRotate(root->left);
             cout<<"ліва-права незбалансованість"<<endl;
             return rightRotate(root);
        }
            cout<<"ліва незбалансованість"<<endl;
            return rightRotate(root);

    }
    return root; // балансування не потрібне.
}
node* insert(node* root, int k) // вставка ключа k в дерево с корнем root
{
    if( !root ) return new node(k);
    if( k<root->key )
        root->left = insert(root->left,k);
    else if(k>root->key)
        root->right = insert(root->right,k);
    return balancing(root);
}

node* findmin(node* root) // пошук вузла з мінімальним ключем в дереві root
{
    return root->left?findmin(root->left):root;
}

node* removemin(node* root) // видалення вузла з найменшим ключем з дерева root(ЗАМІсть видаленого виводим його його місце праву дитину)
{
    if(root->left==NULL)
    return root->right;
    root->left = removemin(root->left);
    return balancing(root);
}

node* remove(node* root, int k) // видалення ключа k из дерева root
{
    if( !root ) return NULL;
    if( k < root->key )
        root->left = remove(root->left,k);
    else if( k > root->key )
        root->right = remove(root->right,k);
    else //  k == root->key
    {
        node* l = root->left;
        node* r = root->right;
        delete root; 
        if( !r ) return l;
        node* min = findmin(r);
        min->right = removemin(r);
        min->left = l;
        return balancing(min);
    }
    return balancing(root);
}
int findMax(node * root)
{
    if(root==NULL)
    {
        abort();
    }
   node* searchMax=root;
   while(searchMax->right!=NULL)
   {
       searchMax=searchMax->right;
   }
   return searchMax->key;

}

