#include<iostream>
#include<iostream>
using namespace std;
struct node // структура для представлення вузлів дерева
{
    int key;
    int height;
    node* left;
    node* right;
    node(int k) { key = k; left = right = 0; height = 0; }
};
int height(node* root);
int bfactor(node* root);
void fixheight(node* root);
node* rightRotate(node* root); // правий поворот навколо root
node* leftRotate(node* root); // лівий поворот навколо root
node* balancing(node* root); // балансування вула root
node* insert(node* root, int k); // вставка ключа k в дерево з корнем root
node* findmin(node* root); // пошук вузла с мінімальным ключем в дереві root
node* removemin(node* root); // видалення вузла с мінімальным ключем із дерева root
node* remove(node* root, int k); // видалення ключа k из дерева root
int findMax(node * root);


