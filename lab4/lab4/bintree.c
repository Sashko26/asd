#include <stdio.h>

#include <stdlib.h>

#include <stdbool.h>



#include "bintree.h"



static int  countNodes(Node *root);

static bool isCompleteRecur(Node *root, int index, int nodes_count);

static bool isHeapRecur(Node *root);

static void printValueOnLevel(Node *node, char pos, int lvl);

static void printNode(Node *node, char pos, int lvl);

static void swap(Node *node1, Node *node2);



Node *newNode(int data)

{

    Node *node = (Node *)malloc(sizeof(Node));



    node->key = data;

    node->right = node->left = NULL;

    return node;

}



void delTree(Node *root)

{

    if (root == NULL)

        return;



    delTree(root->left);

    delTree(root->right);

    free(root);

}



bool isComplete(Node *root)

{

    return isCompleteRecur(root, 0, countNodes(root));

}



bool isBinHeap(Node *root)

{

    if (isComplete(root) && isHeapRecur(root))

        return true;



    return false;

}



void minHeapify(Node *root, Node *prev)

{

    // Post-order

    if (root == NULL) return;      

    minHeapify(root->left, root);

    minHeapify(root->right, root);



    if (prev != NULL && root->key < prev->key) swap(root,prev);   

}



void print(Node *root)

{

    printNode(root, '.', 0); 

}



static int countNodes(Node * root) 

{ 

    if (root == NULL) return 0;



    return    countNodes(root->right); 

} 



static bool isCompleteRecur(Node * root, int index, int nodes_count) 

{ 

    if (root == NULL) return true;  // empty == complete

    if (index >= nodes_count) return false; 

  

    return isCompleteRecur(root->left, 2*index  , nodes_count) && 

                isCompleteRecur(root->right, 2*index  , nodes_count); 

} 





static bool isHeapRecur(Node * root)

{

    if (root->left == NULL && root->right == NULL) return true;  // only root => isHeap

  

    if (root->right == NULL)  // last level

    { 

        return root->key <= root->left->key; 

    } 

    else

    { 

        if (root->key <= root->left->key & root->key <= root->right->key) 

            return isHeapRecur(root->left) && isHeapRecur(root->right); 

        else

            return false; 

    } 

}



static void printNode(Node * node, char pos, int lvl)

{  

   bool hasChild = (node != NULL && (node->left != NULL || node->right != NULL));

   if (hasChild) printNode(node->right, 'R', lvl  );

   printValueOnLevel(node, pos, lvl);

   if (hasChild) printNode(node->left,  'L', lvl  );

}



static void printValueOnLevel(Node * node, char pos, int lvl)

{

   for (int i = 0; i < lvl; i) {

       printf("....");

   }

   printf("%c: ", pos);



   if (node == NULL) {

       printf("(null)\n");

   } else {

       printf("%i\n", node->key);

   }

}



static void swap(Node * node1, Node * node2)

{   

    int temp = node1->key;

    node1->key = node2->key;

    node2->key = temp;

}