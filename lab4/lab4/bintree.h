#pragma once



typedef struct __BinTreeNode Node;



struct __BinTreeNode 

{ 

    int key; 

    Node *left; 

    Node *right; 

}; 





Node * newNode(int data);

void   delTree(Node * root);



bool   isComplete(Node * root);

bool   isBinHeap(Node * root);

void   minHeapify(Node * root, Node * prev);



void   print(Node * root);