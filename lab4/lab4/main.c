#include <stdlib.h>

#include <stdbool.h>

#include <assert.h>



#include "bintree.h"



void tests();

Node *tree();



int main()

{

    // tests();

    Node *root = tree();

    if (root == NULL)

    {

        printf("Bye\n");

        return 0;

    }



    print(root);

    printf("\n\n");

    //



    if (!isComplete(root))

    {

        printf("Tree is not complete\n");

        return 1;

    }



    if (isBinHeap(root))

        printf("Is min heap\n");

    else

    {

        printf("Isn't min heap\n");

        printf("Creating min heap...\n\n");

        minHeapify(root, NULL);

        printf("Binary Heap:\n");

        print(root);

    }



    delTree(root);

    return 0;

}



void tests()

{

    Node *root = newNode(1);

    root->left = newNode(4);

    root->right = newNode(7);

    root->left->left = newNode(8);

    root->left->right = newNode(11);

    root->right->left = newNode(8);

    root->right->right = newNode(9);

    assert(isComplete(root));

    assert(isBinHeap(root));



    free(root->left->left);

    root->left->left = NULL;

    assert(!isComplete(root));

    root->left->left = newNode(2);

    assert(!isBinHeap(root));



    minHeapify(root, NULL);

    assert(isBinHeap(root));

    delTree(root);

}



Node *user_entered();

Node *exampleTree();



Node *tree()

{

    system("clear");

    bool running = true;

    int choice = 0;

    while (running)

    {

        printf("Choose your tree: \n");

        printf("1. Enter it yourself\n");

        printf("2. Try example tree\n");

        printf("0. Quit\n");



        scanf("%i", &choice);



        switch (choice)

        {

        case 1:

        {

            return user_entered();

            break;

        }

        case 2:

        {

            return exampleTree();

        }

        case 0:

        {

            system("clear");

            return NULL;

        }

        default:

        {

            system("clear");

            printf("Try again\n");

        }

        }

    }

    return NULL;

}





void takeInput(Node ** node) {



    int left;

    int right;

    int data;

    printf("Enter root: ");

    scanf("%i", &data);

    *node = newNode(data);



    printf("Node(%i): Does left node exist? \t\t1 - YES, 0 - NO: ", data);

    scanf("%i", &left);

    if (left == 1)

    {

        takeInput(&(*node)->left);

    }



    printf("Node(%i): Does right node exist? \t1 - YES, 0 - NO: ", data);

    scanf("%i", &right);

    if (right == 1)

    {

        takeInput(&(*node)->right);

    }

}



Node* InputBinaryTree() {

    Node * root = NULL;

    takeInput(&root);

    return root;

}



Node *user_entered()

{

    system("clear");

    printf("Enter tree:\n");

    Node * root = InputBinaryTree();

    return root;

}



Node *exampleTree()

{

    Node *root = newNode(3);

    root->left = newNode(16);

    root->right = newNode(1);



    root->left->left = newNode(8);

    root->left->right = newNode(11);



    root->right->left = newNode(8);

    root->right->right = newNode(24);



    return root;

}