
#include "AVLtree.h"
#include "assert.h"
#include <string.h>
void print(node *root);
void mainTest();
int main(int argc, char *argv[])
{

  if (argc > 1 && strcmp(argv[1], "-t") == 0)
  {
    mainTest();
  }
  else
  {
    node *root = NULL;
    bool work = true;
    while (work)
    {
      puts("1.Введіть значення.");
      puts("2.Завершіть роботу.");
      char ch;
      cin >> ch;
      while (ch != '1' && ch != '2')
      {
        puts("Введений некоректний символ.Спробуйте будь ласка ще раз");
        cin >> ch;
      }
      if (ch == '1')
      {
        puts("введіть нове числове значення:");
        int value;
        cin >> value;
        root = insert(root, value);
        print(root);
        puts("");
      }
      else if (ch == '2')
      {
        print(root);
        puts("");
       int max = findMax(root);
        root = remove(root, max);
        print(root);
        puts("");
        work = false;
      }
    }
  }
  return 0;
}

void mainTest()
{
  node *root = NULL;
  root = insert(root, 1);
  assert(root->key == 1);
  root = insert(root, 6);
  root = insert(root, 3);
  root = insert(root, 4);
  root = insert(root, 5);
  assert(root->right->key == 5);
  assert(root->key == 3);
  root = remove(root, 3);
  assert(findMax(root) == 6);
  assert(findMax(root) == 6);
  assert(findmin(root)->key == 1);
  assert(bfactor(root) == -1);
}
void print(node *root)
{
  if (root == NULL)
    return;
  print(root->left);
  cout << "|" << root->key << "|"
       << " ";
  print(root->right);
}
