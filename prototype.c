#include <stdio.h>  // Для друку в термінал
#include <math.h>   // Для математичних функцій
#include <stdlib.h> // Деякі додаткові функції
#include <progbase.h>
#include <progbase/console.h>
#include <string.h>
#include <assert.h>
#include <time.h>

//cтруктура односпрямованого елементу
typedef struct SLNode
{
    int data;
    struct SLNode *next;

} SLNode;

//cтруктура двоспрямованого елементу
typedef struct DLNode
{
    int data;
    struct DLNode *next;
    struct DLNode *prev;
} DLNode;

//прототипи
SLNode *createSLNode(int data);
DLNode *createDLNode(int data);
DLNode *addDLNode(DLNode *head, DLNode *node);
SLNode *addSLNode(SLNode *head, SLNode *node);
void printSLList(SLNode *list);
void printDLList(DLNode *list);
int sizeSL(SLNode *list);
int sizeDL(DLNode *list);
SLNode *createSecondList(DLNode *dlHead);
SLNode *createSLNodefromDLNode(DLNode *node);
SLNode *TryTofreeHeapfromSLlist(SLNode *SLlist);
DLNode *TryTofreeHeapfromDLlist(DLNode *DLlist);
int maintest();

int main(int argc, char *argv[])
{
    if (argc >= 2 && strcmp(argv[1], "-t") == 0)
    {
        maintest();
    }

    else
    {
        srand(time(NULL));
        //створення вузлів двоспрямованого виду
        int n = 0;
        DLNode *DLlist = createDLNode(rand() % 10);
        printf("введіть кількість елементів: ");
        scanf("%i", &n);
        Console_clear();
        //якщо користувач вводить кількість елементів списку менше 0 або менше нуля
        while (n <= 0)
        {
            Console_clear();
            printf("введіть, будь ласка, коректне значення кількості елементів: ");
            scanf("%i", &n);
        }
        //якщо кількість елементів списку дорівнює 1
        if (n == 1)
        {
            Console_clear();
            printf("Елементи двоспрямованого списку: ");
            printDLList(DLlist);
            printf("Кількість елементів двоспрямованого списку: ");

            printf("%i\n", sizeDL(DLlist));
            free(DLlist);
        }
        else
        {
            for (int i = 1; i < n; i++)
            {
                DLlist = addDLNode(DLlist, createDLNode(rand() % 10));
            }

            //виведення двоспрямованого списку в консоль а
            //також виведення кількості вузлів двоспрямованого списку
            printf("Елементи двоспрямованого списку: ");
            printDLList(DLlist);
            printf("Кількість елементів двоспрямованого списку: ");
            printf("%i\n", sizeDL(DLlist));

            //створення односпрямованого списку на основі двоспрямованого,
            //виведення його елементів в консоль, виведення кількості його елементів в консоль
            SLNode *SLlistFromDLlist = createSecondList(DLlist);
            printf("Елементи односпрямованого списку : ");
            printSLList(SLlistFromDLlist);
            printf("Кількість елементів односпрямованого списку: ");
            printf("%i\n", sizeSL(SLlistFromDLlist));

            //виведення в консоль оновленого(наполовину видаленого)
            // двоспрямованого списку, виведення кількості його елементів
            printf("Елементи двоспрямованого списку: ");
            printDLList(DLlist);
            printf("Кількість елементів двоспрямованого списку: ");
            printf("%i\n ", sizeDL(DLlist));
            //очищення виділеної динамічної пам'яті
            DLNode *p = DLlist;
            SLNode *pointer = SLlistFromDLlist;
            while (DLlist != NULL)
            {
                p = DLlist->next;
                free(DLlist);
                DLlist = p;
            }
            while (SLlistFromDLlist != NULL)
            {
                pointer = SLlistFromDLlist->next;
                free(SLlistFromDLlist);
                SLlistFromDLlist = pointer;
            }
        }
    }
}

//Реалізації функцій
//cтворення нового елемента двоспрямованого списку
DLNode *createDLNode(int data)
{
    DLNode *head = malloc(sizeof(DLNode));
    head->data = data;
    head->next = NULL;
    head->prev = NULL;
    return head;
}
// добавляння нового елементу до двоспрямованого списку
DLNode *addDLNode(DLNode *head, DLNode *node)
{
    if (head == NULL && node == NULL)
    {
        return NULL;
    }
    else if (head == NULL)
    {
        head = node;
    }
    else
    {
        DLNode *p = head;
        int count = 0;
        while (p)
        {
            count++;
            p = p->next;
        }

        if (count % 2 == 1)
        {
            node->next = head;
            node->prev = NULL;
            head->prev = node;
            head = node;
        }

        else
        {
            p = head;
            int Newcount = 0;
            while (Newcount != count / 2)
            {
                Newcount++;
                p = p->next;
            }

            node->next = p;
            node->prev = p->prev;
            node->next->prev = node;
            node->prev->next = node;
        }
    }
    return head;
}

// виведення в консоль двоспрямованого списку
void printDLList(DLNode *list)
{
    DLNode *p = list;
    while (p)
    {
        printf("%i ", p->data);
        p = p->next;
    }
    puts("");
}
//виведення односпрямованого списку в консоль
void printSLList(SLNode *list)
{
    while (list)
    {
        printf("%i ", list->data);
        list = list->next;
    }
    puts("");
}

//створення нового елемента односпрямованого списку
SLNode *createSLNode(int data)
{
    SLNode *head = malloc(sizeof(SLNode));
    head->data = data;
    head->next = NULL;
    return head;
}
// добавлення нового елемента до односпрямованого списку
SLNode *addSLNode(SLNode *head, SLNode *node)
{
    node->next = head;
    head = node;
    return head;
}
//створення односпрямованої ноди на основі данних двоспрямованої ноди, тобто створення голови
SLNode *createSLNodefromDLNode(DLNode *dlHead)
{
    SLNode *head = malloc(sizeof(SLNode));
    head->data = dlHead->data;
    head->next = NULL;
    return head;
}
//звільнення динамічної пам'яті від двоспрямованого списку
DLNode *TryTofreeHeapfromDLlist(DLNode *DLlist)
{
    while (DLlist != NULL)
    {
        DLNode *helperDLNode = DLlist;
        DLlist = DLlist->next;
        free(helperDLNode);
    }

    return DLlist;
}

//звільнення динамічної пам'яті від односпрямованого списку списку
SLNode *TryTofreeHeapfromSLlist(SLNode *SLlist)
{
    while (SLlist != NULL)
    {
        SLNode *helperSLNode = SLlist;
        SLlist = SLlist->next;
        free(helperSLNode);
    }

    return SLlist;
}

//створення односпрямованого списку на основі видалених елементів з
//двоспрямованого списку визначених за варіантом(друга половина)
SLNode *createSecondList(DLNode *node)
{
    int count = 0;
    DLNode *p = node;
    while (p)
    {
        count++;
        p = p->next;
    }
    if (count == 1)
    {
        return NULL;
    }
    else
    {
        p = node;
        int newCount = 0;
        while (newCount != count / 2)
        {
            newCount++;
            p = p->next;
        }
        p->prev->next = NULL;
        p->prev = NULL;
        SLNode *NewSLnode;
        SLNode *headOfSLlist = createSLNodefromDLNode(p);
        DLNode *rememberPforFREE = p;
        p = p->next;
        while (p)
        {
            NewSLnode = createSLNodefromDLNode(p);
            headOfSLlist = addSLNode(headOfSLlist, NewSLnode);
            p = p->next;
        }
        DLNode *headofdeletedPartofList = rememberPforFREE;
        while (headofdeletedPartofList != NULL)
        {
            rememberPforFREE = headofdeletedPartofList->next;
            free(headofdeletedPartofList);
            headofdeletedPartofList = rememberPforFREE;
        }

        return headOfSLlist;
    }
}
//обчислює і повертає кількість вузлів
//в односпрямованому зв’язаному списку.
int sizeSL(SLNode *list)
{
    int count = 0;
    while (list)
    {
        count++;
        list = list->next;
    }
    return count;
}
//обчислює і повертає кількість вузлів
//в двоспрямованому зв’язаному списку.
int sizeDL(DLNode *list)
{
    int count = 0;
    while (list)
    {
        count++;
        list = list->next;
    }
    return count;
}

//функція тестувальник
int maintest()
{
    //перевірка функції створення односпрямованої ноди на основі данних двоспрямованої
    DLNode *DL = createDLNode(10);
    SLNode *SL = createSLNodefromDLNode(DL);
    assert(SL->data == 10);
    free(DL);
    free(SL);
    //створення вузлів двоспрямованого виду
    DLNode *DLlist = createDLNode(rand() % 10);
    int n = rand() % 10;

    // створення масиву для зберігання послідовності елементів списку
    int arrayOfelementsDLlist[n];

    //додавання елементу голови списку в масив
    arrayOfelementsDLlist[0] = DLlist->data;
    DLNode *bufDLNode;

    //створення двостороннього списку і заповнення масиву значеннями
    for (int i = 1; i < n; i++)
    {
        bufDLNode = createDLNode(rand() % 10);
        arrayOfelementsDLlist[i] = bufDLNode->data;
        DLlist = addDLNode(DLlist, bufDLNode);
    }

    //буферний вузол, за допомогою якого ми проходим список
    bufDLNode = DLlist;
    //створення змінних,за допомогою яких елементи списку порівнюються з відповідними їм елементами масиву
    int positionLess = n - 2;
    int positionMore = n - 1;
    //перевірка функції добавляння вузлів в двоспрямований список
    if (sizeDL(DLlist) % 2 == 0)
    {
        while (positionMore >= 1)
        {
            assert(arrayOfelementsDLlist[positionMore] == bufDLNode->data);
            bufDLNode = bufDLNode->next;
            positionMore = positionMore - 2;
        }
        while (positionLess >= 0)
        {
            assert(arrayOfelementsDLlist[positionLess] == bufDLNode->data);
            bufDLNode = bufDLNode->next;
            positionLess = positionLess - 2;
        }
    }
    else if (sizeDL(DLlist) % 2 == 1)
    {
        while (positionLess >= 1)
        {
            assert(arrayOfelementsDLlist[positionLess] == bufDLNode->data);
            bufDLNode = bufDLNode->next;
            positionLess = positionLess - 2;
        }
        while (positionMore >= 0)
        {
            assert(arrayOfelementsDLlist[positionMore] == bufDLNode->data);
            bufDLNode = bufDLNode->next;
            positionMore = positionMore - 2;
        }
    }

    //перевірка функції визначення розміру двоспрямованого списку
    assert(sizeDL(DLlist) == n);

    //cтворення односпрямованого списку на основі двоспрямованого
    SLNode *SLlist = createSecondList(DLlist);

    // cтворення буферного вказівника для перевірки функції створення односпрямованого списку
    SLNode *pointer = SLlist;

    //перевірка функції створення односпрямованого списку на основі видалених елементів з
    //двоспрямованого списку визначених за варіантом(друга половина)

    int i = 0;
    while (pointer != NULL)
    {
        assert(pointer->data == arrayOfelementsDLlist[i]);
        pointer = pointer->next;
        i = i + 2;
    }

    //перевірка функції визначення розміру односпрямованого  списку
    assert(sizeSL(SLlist) == n - n / 2);

    //звільнення динамічної пам'яті виділеної для списків DLlist,SLlist
    TryTofreeHeapfromDLlist(DLlist);
    TryTofreeHeapfromSLlist(SLlist);

    //перевірка випадку, коли під критерій видалення не підпадає жоден елемент списку(за моїм варіантом це тоді,
    //коли двосторонній список містить лише один елемент)
    DLNode *DLlist2 = createDLNode(rand()%10);
    assert(createSecondList(DLlist2) == NULL);
    TryTofreeHeapfromDLlist(DLlist2);

    puts("Nice job!");
    return 0;
}